#include "Face.h"
#include "HalfEdge.h"
#include "Vertex.h"
#include <cstring>
#include <string>

#define __FILENAME__                                                           \
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

Face::Face(std::size_t id)
    : half_edge_(nullptr), id_(id), normal_(0), area_(0), center_(0),
      removed_(false) {}

// TODO
const std::vector<HalfEdge *> Face::getAdjacentHalfEdges() const {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const std::vector<Vertex *> Face::getAdjacentVertices() const {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const Vector3d Face::calcNormal() {
  // TODO calculate and populate normal_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const double Face::calcArea() {
  // TODO calculate and populate area_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const Vector3d Face::calcCenter() {
  // TODO calculate and populate center_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}
