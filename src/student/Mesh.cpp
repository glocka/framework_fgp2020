#include "Mesh.h"
#include "../helper/FileHandler.h"
#include "../helper/Point.h"
#include "../helper/Triangle.h"
#include "Face.h"
#include "HalfEdge.h"
#include "Vertex.h"
#include <algorithm>
#include <cstring>
#include <iostream>
#include <limits>

#define __FILENAME__                                                           \
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

Mesh::Mesh()

    : face_counter_(0), half_edge_counter_(0), vertex_counter_(0),
      min_distance_(0), max_distance_(0), removed_vertices_count_(0),
      removed_faces_count_(0) {}

Mesh::~Mesh() {
  auto del = [](auto x) { delete x; };
  std::for_each(vertices_.begin(), vertices_.end(), del);
  std::for_each(faces_.begin(), faces_.end(), del);
  std::for_each(half_edges_.begin(), half_edges_.end(), del);
}

void Mesh::load(const std::string &filename) {
  mesh_name_ = filename;
  if (mesh_name_.rfind('/') != std::string::npos)
    mesh_name_ = mesh_name_.substr(mesh_name_.rfind('/') + 1);

  std::vector<std::shared_ptr<Point>> point_vec;
  std::vector<std::shared_ptr<Colour>> color_vec;
  std::vector<std::shared_ptr<Triangle>> poly_vec;
  FileHandler::readFromPLYFile(point_vec, color_vec, poly_vec, filename);
  load(point_vec, color_vec, poly_vec);
}

// TODO
void Mesh::load(std::vector<std::shared_ptr<Point>> &point_vec,
                std::vector<std::shared_ptr<Colour>> &color_vec,
                std::vector<std::shared_ptr<Triangle>> &poly_vec) {
  // TODO fill vectors vertices_, faces_, and half_edges_. Use getNewVertexID(),
  // getNewFaceID(), and getNewHalfEdgeID()
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
Vertex *Mesh::getVertexAtPosition(const Point &pos) {
  // TODO return nullptr if there is no vertex at the given position
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
void Mesh::smooth(double lambda) {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
void Mesh::decimate() {
  // TODO calculate and populate min_distance_ and max_distance_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
Vertex *Mesh::findSuitableVertex() {
  // TODO return nullptr if there is no suitable vertex
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
void Mesh::removeVertex(Vertex *vertex) {
  // TODO use HalfEdge::remove(), Face::remove(), Vertex::remove(),
  // removed_faces_count_, and removed_vertices_count_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}
