#include "Vertex.h"
#include "../helper/Point.h"
#include "Colour.h"
#include "Face.h"
#include "HalfEdge.h"
#include "Util.h"
#include <algorithm>
#include <cmath>
#include <cstring>
#include <string>
#include <utility>

#define __FILENAME__                                                           \
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

Vertex::Vertex(const std::shared_ptr<Point> &pos,
               const std::shared_ptr<Colour> &color, std::size_t id)
    : pos_(std::move(pos)), half_edge_(), id_(id),
      color_(color->getR(), color->getG(), color->getB()), roi_area_(0),
      la_place_sum_(0, 0, 0), la_place_operator_(0, 0, 0), average_normal_(0),
      average_point_(0), distance_(0), removed_(false) {}

// TODO
const std::vector<HalfEdge *> Vertex::getAdjacentHalfEdges() const {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const std::vector<Face *> Vertex::getAdjacentFaces() const {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const std::vector<Vertex *> Vertex::getOneRingNeighbourhood() const {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const bool Vertex::isVertexAtPosition(const Point &pos) const {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const double Vertex::calcROIArea() {
  // TODO calculate and populate roi_area_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const Colour Vertex::calcLaPlaceSum() {
  // TODO calculate and populate la_place_sum_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const Colour Vertex::calcLaPlaceOperator() {
  // TODO calculate and populate la_place_operator_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
void Vertex::applyLaPlaceOperator(const double h, const double lambda) {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const Vector3d Vertex::calcAverageNormal() {
  // TODO calculate and populate average_normal_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const Vector3d Vertex::calcAveragePoint() {
  // TODO calculate and populate average_point_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const double Vertex::calcDistanceToAveragePlane() {
  // TODO calculate and populate distance_
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}

// TODO
const bool Vertex::isInConvexNeighbourhood() {
  throw std::runtime_error("TODO: Implement " + std::string(__FUNCTION__) +
                           "(...) in " + std::string(__FILENAME__));
}
